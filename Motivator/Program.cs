﻿using System;

namespace Motivator
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Программа создания мотиватора.");
                Console.Write("Введите полный путь к изображению мотиватора: ");
                string motivatorImagePath = Console.ReadLine();

                Console.Write("Введите текст мотиватора: ");
                string motivationText = Console.ReadLine();

                var fileName = ImageGenerator.CreateMotivator(motivatorImagePath, motivationText);
                Console.WriteLine($"Полный путь к мотиватору: {fileName}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
