﻿using System.Drawing;
using System.Drawing.Drawing2D;

namespace Motivator
{
    /// <summary>
    /// Класс отвечает за генерацию мотивационного изображения.
    /// </summary>
    internal class MotivationImageDecorator : BaseDecorator
    {
        private Image _motivationImage;
        private readonly int _borderX = 5;
        private readonly int _borderY = 5;

        public MotivationImageDecorator(string motivationImagePath, IComponent component) : base(component)
        {
            // Подгружаем изображение с диска.
            _motivationImage = Image.FromFile(motivationImagePath);
        }

        /// <summary>
        /// Метод отвечает за отрисовку компонента.
        /// </summary>
        /// <param name="image">Модифицируемое изображение.</param>
        public override void Draw(Image image)
        {
            using (Graphics graphics = Graphics.FromImage(image))
            {
                Size newSize = new Size(image.Width - (50 + _borderX) * 2, image.Height - (100 + _borderY) * 2);

                // Подгоняем изображение мотиватора под нужное.
                _motivationImage = ResizeImage(_motivationImage, newSize);
                Point point = new Point(50 + _borderX, 50 + _borderY);
                graphics.DrawImage(_motivationImage, point);
            }
            base.Draw(image);
        }

        /// <summary>
        /// Метод меняет размер изображения.
        /// </summary>
        /// <param name="image">Модифицируемое изображение.</param>
        /// <param name="newSize">Новый размер.</param>
        /// <returns>Модифицированное изображение.</returns>
        private Image ResizeImage(Image image, Size newSize)
        {
            Image newImage = new Bitmap(newSize.Width, newSize.Height);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(image, 0, 0, newSize.Width, newSize.Height);
            }
            return newImage;
        }
    }
}
