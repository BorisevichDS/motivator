﻿using System.Drawing;

namespace Motivator
{
    /// <summary>
    /// Класс отвечает за генерацию фонового изображения.
    /// </summary>
    internal class BackgroundDecorator : BaseDecorator
    {
        public BackgroundDecorator(IComponent component) : base(component)
        {
        }

        /// <summary>
        /// Метод отвечает за отрисовку компонента.
        /// </summary>
        /// <param name="image">Модифицируемое изображение.</param>
        public override void Draw(Image image)
        {
            using (Graphics graphics = Graphics.FromImage(image))
            {
                graphics.FillRectangle(Brushes.CornflowerBlue, 0, 0, image.Height, image.Width);
            }
            base.Draw(image);
        }
    }
}
