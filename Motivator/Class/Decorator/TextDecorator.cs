﻿using System;
using System.Drawing;

namespace Motivator
{
    /// <summary>
    /// Класс отвечает за генерацию текста мотиватора.
    /// </summary>
    internal class TextDecorator : BaseDecorator
    {
        private string _text;
        private int _fontSize = 30;

        public TextDecorator(string text, IComponent component) : base(component)
        {
            _text = text;
        }

        /// <summary>
        /// Метод отвечает за отрисовку компонента.
        /// </summary>
        /// <param name="image">Модифицируемое изображение.</param>
        public override void Draw(Image image)
        {
            using (Graphics graphics = Graphics.FromImage(image))
            {
                /*
                    За основу берем размер изображения 800х800, для такого изображения размер шрифта 30.
                    Если размер изображения меньше или больше, то корректируем размер шрифта относительно
                    размера изображения.
                */
                _fontSize = (int)(image.Width / 800.0 * 30.0);
                Font font = new Font("Arial", _fontSize, GraphicsUnit.Point);
                SolidBrush brush = new SolidBrush(Color.Black);

                var stringSize = graphics.MeasureString(_text, font);
                PointF point = new PointF((image.Width - stringSize.Width) / 2, image.Height - 100);

                graphics.DrawString(_text, font, brush, point);
            }
            base.Draw(image);
        }
    }
}
