﻿using System;
using System.Drawing;

namespace Motivator
{
    internal abstract class BaseDecorator : IComponent
    {
        private IComponent _component;

        public BaseDecorator(IComponent component)
        {
            _component = component;
        }

        /// <summary>
        /// Метод отвечает за отрисовку компонента.
        /// </summary>
        /// <param name="image">Модифицируемое изображение.</param>
        public virtual void Draw(Image image)
        {
            _component?.Draw(image);
        }
    }
}
