﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Motivator
{
    /// <summary>
    /// Класс отвечате за генерацию рамки.
    /// </summary>
    internal class FrameDecorator : BaseDecorator
    {
        public FrameDecorator(IComponent component) : base(component)
        {
        }

        /// <summary>
        /// Метод отвечает за отрисовку компонента.
        /// </summary>
        /// <param name="image">Модифицируемое изображение.</param>
        public override void Draw(Image image)
        {
            using (Graphics graphics = Graphics.FromImage(image))
            {
                // width = 800 - 100 - 50 = 650, height = 800 - 200 - 50 = 550
                Rectangle rectangle = new Rectangle(50, 50, image.Width - 50 * 2, image.Height - 100 * 2);
                graphics.FillRectangle(Brushes.White, rectangle);
            }
            base.Draw(image);
        }
    }
}
