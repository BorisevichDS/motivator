﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace Motivator
{
    /// <summary>
    /// Генератор изображений.
    /// </summary>
    internal static class ImageGenerator
    {
        /// <summary>
        /// Метод создает мотиватор.
        /// </summary>
        /// <param name="motivationImagePath">Путь к изображению для мотиватора.</param>
        /// <param name="message">Текст мотиватора.</param>
        /// <returns>Название созданного файла.</returns>
        public static string CreateMotivator(string motivationImagePath, string message)
        {
            Bitmap image = new Bitmap(800, 800);

            IComponent component = new BaseComponent();

            IComponent decorator = new MotivationImageDecorator(motivationImagePath, component);
            decorator = new TextDecorator(message, decorator);
            decorator = new FrameDecorator(decorator);
            decorator = new BackgroundDecorator(decorator);

            decorator.Draw(image);

            if (!Directory.Exists(Directory.GetCurrentDirectory() + @"\Motivator\"))
            {
                Directory.CreateDirectory(@"Motivator");
            }
            string path = Directory.GetCurrentDirectory() + @"\Motivator\" + "Motivator.png";
            image.Save(path, ImageFormat.Png);

            return path;
        }

    }
}
