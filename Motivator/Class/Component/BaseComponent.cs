﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Motivator
{
    /// <summary>
    /// Базовый компонент.
    /// </summary>
    class BaseComponent : IComponent
    {
        /// <summary>
        /// Метод отвечает за отрисовку компонента.
        /// </summary>
        /// <param name="image">Модифицируемое изображение.</param>
        public void Draw(Image image)
        {
            
        }
    }
}
