﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Motivator
{
    internal interface IComponent
    {
        /// <summary>
        /// Метод отвечает за отрисовку компонента.
        /// </summary>
        /// <param name="image">Модифицируемое изображение.</param>
        void Draw(Image image);
    }
}
